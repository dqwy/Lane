package com.sc.service;

import java.util.Iterator;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

public class LocationService extends Service{
	private static final String TAG = "LocationService";
	private LocationManager mLocManager;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override  
    public void onCreate() {  
        Log.v(TAG, "onCreate");  
        mLocManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		// 判断GPS是否正常启动
		if (!mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			return;
		}

		// 为获取地理位置信息时设置查询条件
		String bestProvider = mLocManager.getBestProvider(getCriteria(), true);
		
		// 获取位置信息
		// 如果不设置查询要求，getLastKnownLocation方法传人的参数为LocationManager.GPS_PROVIDER
		Location location = mLocManager.getLastKnownLocation(bestProvider);
		
		broadcastLocationData(location);
		
		// 监听状态
		mLocManager.addGpsStatusListener(gpsStatuslistener);
		// 绑定监听，有4个参数
		// 参数1，设备：有GPS_PROVIDER和NETWORK_PROVIDER两种
		// 参数2，位置信息更新周期，单位毫秒
		// 参数3，位置变化最小距离：当位置距离变化超过此值时，将更新位置信息
		// 参数4，监听
		// 备注：参数2和3，如果参数3不为0，则以参数3为准；参数3为0，则通过时间来定时更新；两者为0，则随时刷新

		// 1秒更新一次，或最小位移变化超过1米更新一次；
		// 注意：此处更新准确度非常低，推荐在service里面启动一个Thread，在run中sleep(10000);然后执行handler.sendMessage(),更新位置
		mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				locationListener);
    }  
  
    @Override  
    public void onDestroy() {  
        Log.v(TAG, "onDestroy");  
        mLocManager.removeUpdates(locationListener);
    }  
  
    @Override  
    public void onStart(Intent intent, int startId) {  
        Log.v(TAG, "onStart");  
        
    } 
    
    private void broadcastLocationData(Location loc){
    	Intent intent1 = new Intent();
        intent1.putExtra("loc", loc);
        intent1.setAction("android.intent.action.loc");//action与接收器相同
        sendBroadcast(intent1);
    }
    
    GpsStatus.Listener gpsStatuslistener = new GpsStatus.Listener() {
		public void onGpsStatusChanged(int event) {
			switch (event) {
			// 第一次定位
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				Log.i(TAG, "第一次定位");
				break;
			// 卫星状态改变
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
				Log.i(TAG, "卫星状态改变");
				// 获取当前状态
				GpsStatus gpsStatus = mLocManager.getGpsStatus(null);
				// 获取卫星颗数的默认最大值
				int maxSatellites = gpsStatus.getMaxSatellites();
				// 创建一个迭代器保存所有卫星
				Iterator<GpsSatellite> iters = gpsStatus.getSatellites()
						.iterator();
				
				int count = 0;
				while (iters.hasNext() && count <= maxSatellites){
					GpsSatellite s = iters.next();
					count++;

				    //卫星的方位角，浮点型数据   
				    System.out.println(s.getAzimuth());  
				    //卫星的高度，浮点型数据   
				    System.out.println(s.getElevation());  
				    //卫星的伪随机噪声码，整形数据   
				    System.out.println(s.getPrn());  
				    //卫星的信噪比，浮点型数据   
				    System.out.println(s.getSnr());  
				    //卫星是否有年历表，布尔型数据   
				    System.out.println(s.hasAlmanac());  
				    //卫星是否有星历表，布尔型数据   
				    System.out.println(s.hasEphemeris());  
				    //卫星是否被用于近期的GPS修正计算   
				    System.out.println(s.hasAlmanac());  
				}
				
				System.out.println("搜索到：" + count + "颗卫星");
				break;
			// 定位启动
			case GpsStatus.GPS_EVENT_STARTED:
				Log.i(TAG, "定位启动");
				break;
			// 定位结束
			case GpsStatus.GPS_EVENT_STOPPED:
				Log.i(TAG, "定位结束");
				break;
			}
		};
	};
    
    private LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			broadcastLocationData(location);
			Log.i(TAG, "时间：" + location.getTime());
			Log.i(TAG, "经度：" + location.getLongitude());
			Log.i(TAG, "纬度：" + location.getLatitude());
			Log.i(TAG, "海拔：" + location.getAltitude());
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			switch (status) {
			// GPS状态为可见时
			case LocationProvider.AVAILABLE:
				Log.i(TAG, "当前GPS状态为可见状态");
				break;
			// GPS状态为服务区外时
			case LocationProvider.OUT_OF_SERVICE:
				Log.i(TAG, "当前GPS状态为服务区外状态");
				break;
			// GPS状态为暂停服务时
			case LocationProvider.TEMPORARILY_UNAVAILABLE:
				Log.i(TAG, "当前GPS状态为暂停服务状态");
				break;
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}
    };
    
    private Criteria getCriteria() {
		Criteria criteria = new Criteria();
		// 设置定位精确度 Criteria.ACCURACY_COARSE比较粗略，Criteria.ACCURACY_FINE则比较精细
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		// 设置是否要求速度
		criteria.setSpeedRequired(false);
		// 设置是否允许运营商收费
		criteria.setCostAllowed(false);
		// 设置是否需要方位信息
		criteria.setBearingRequired(true);
		// 设置是否需要海拔信息
		criteria.setAltitudeRequired(true);
		// 设置对电源的需求
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		return criteria;
	}
}
